import setuptools

setuptools.setup(
    name="msr",
    version="0.0.1",
    author="William Meng",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'msr = msr:cli'
        ]
    }
)

