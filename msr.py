import fire
import csv
import sys
import os
import validators
import threading
from waiting import wait
from tabulate import tabulate
import requests
from datetime import datetime
from xdg import (
    xdg_cache_home,
    xdg_config_dirs,
    xdg_config_home,
    xdg_data_dirs,
    xdg_data_home,
    xdg_runtime_dir,
)

def measure_request(url: str, csv_writer):
    start = datetime.now()
    response = requests.get(url)
    end = datetime.now()
    req_time = end - start
    size = len(response.content)
    csv_writer.writerow([url, size, req_time])
    return size

class measurementThread (threading.Thread):
    def __init__(self, url, csv_writer):
        threading.Thread.__init__(self)
        self.url = url
        self.csv_writer = csv_writer
    def run(self):
        try:
            measure_request(self.url, self.csv_writer)
        except:
            pass

class Msr(object):
    def version(self):
        return "0.0.1"

    def register(self, url: str):
        try:
            is_url = validators.url(url)
            if (is_url is not True):
                raise is_url

            root_data_dir = xdg_data_home()
            app_data_dir = os.path.join(root_data_dir, "msr")
            registry_file_path = os.path.join(app_data_dir, "registry.txt")

            if not os.path.exists(app_data_dir):
                os.makedirs(app_data_dir)
            
            registry = open(registry_file_path, "r+")
            csv_reader = csv.reader(registry)
            csv_writer = csv.writer(registry)
            already_exists = False
            for row in csv_reader:
                if row == [url]:
                    already_exists = True
            if not already_exists:
                csv_writer.writerow([url])
        except Exception as e:
            sys.exit("Invalid URL")
    
    def measure_or_race(self, function_type: str):
        root_data_dir = xdg_data_home()
        registry_file_path = os.path.join(root_data_dir, "msr/registry.txt")
        measurements_file_path = os.path.join(root_data_dir, "msr/measurements.txt")
        try:
            registry = open(registry_file_path, "r")
            measurements = open(measurements_file_path, "w+")
            registry_reader = csv.reader(registry)
            measurement_writer = csv.writer(measurements)
            measurement_reader = csv.reader(measurements)
        except Exception as e:
            return 1
        results = []

        for row in registry_reader:
            # Start a new thread for GET request
            url = row[0]
            measurement_thread = measurementThread(url, measurement_writer)
            measurement_thread.start()
        
        wait(lambda: threading.activeCount() == 1) # wait for threads to finish
        measurements.seek(0)
        if (function_type == 'measure'):
            for row in measurement_reader:
                results.append(row[0:2])
            print(tabulate(results, headers=["URL", "Size"]))
        else:
            for row in measurement_reader:
                results.append([row[0],row[2]])
            print(tabulate(results, headers=["URL", "Time"]))
    
    def measure(self):
        self.measure_or_race('measure')
    
    def race(self):
        self.measure_or_race('race')


def cli():
    fire.Fire(Msr)